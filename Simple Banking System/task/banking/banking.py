# Write your code here
import math
import random
import sqlite3

state = True
issuer_ident_num = '400000'
accounts = {}
conn = sqlite3.connect('card.s3db')
cur = conn.cursor()
cur.execute('CREATE TABLE IF NOT EXISTS card (id INTEGER, number TEXT, pin TEXT, balance INTEGER DEFAULT 0);')
conn.commit()


def luhn_algo(acc_num):
    sum_num = 0
    for i in range(0, 15):
        if i % 2 == 0:
            double = int(acc_num[i]) * 2
            if double > 9:
                double = double - 9
            sum_num = sum_num + double
        else:
            sum_num = sum_num + int(acc_num[i])
    return (10 - (sum_num % 10)) % 10


def is_luhn(acc_num):
    return luhn_algo(acc_num) == acc_num[15]


def get_new_num():
    new_num = issuer_ident_num
    for _ in range(0, 9):
        new_num = new_num + str(random.randint(0, 9))
    new_num = new_num + str(luhn_algo(new_num))
    return new_num


def get_valid_num():
    global accounts
    new_num = get_new_num()
    while new_num in accounts:
        new_num = get_new_num()
    return new_num


def get_pin():
    pin = ''
    for _ in range(0, 4):
        pin = pin + str(random.randint(0, 9))
    return pin


def create_acc():
    global accounts
    acc_num = get_valid_num()
    acc_pin = get_pin()
    accounts[acc_num] = acc_pin
    cur.execute('INSERT INTO card (number, pin) VALUES (?, ?);', (acc_num, acc_pin))
    conn.commit()
    print('Your card has been created')
    print(f'Your card number:\n{acc_num}')
    print(f'Your card PIN:\n{acc_pin}')

def fetch_bal(acc_num):
    cur.execute('SELECT balance FROM card WHERE number = (?)', (acc_num,))
    fetched_bal = cur.fetchone()
    return fetched_bal[0]


def card_exists(acc_num):
    cur.execute('SELECT COUNT(*) FROM card WHERE number = ?', (acc_num,))
    count = cur.fetchone()
    return int(count[0]) == 1


def login():
    global accounts
    global state
    print('Enter your card number:')
    acc_num = str(input())
    print('Enter your PIN:')
    pin = str(input())
    if acc_num not in accounts:
        print('wrong')
    else:
        cur.execute('SELECT pin FROM card WHERE number = (?)', (acc_num,))
        fetched_pin = cur.fetchone()
        fetched_pin = fetched_pin[0]
        if fetched_pin == pin:
            print('You have successfully logged in!')
            while True:
                print('1. Balance\n2. Add Income\n3. Do transfer\n4. Close account\n5. Log out\n0. Exit')
                choice = int(input())
                if choice == 1:
                    print(f'Balance: {fetch_bal(acc_num)}')
                elif choice == 2:
                    print('Enter income:')
                    income = int(input())
                    cur.execute('UPDATE card SET balance = balance + ? WHERE number = ?', (income,acc_num))
                    conn.commit()
                    print('Income was added!')
                elif choice == 3:
                    print('Transfer\nEnter card number:')
                    trans_num = str(input())
                    if not is_luhn(trans_num):
                        print('Probably you made a mistake in the card number. Please try again!')
                    if not card_exists(trans_num):
                        print('Such a card does not exist')
                    else:
                        print('Enter how much money you want to transfer:')
                        trans = int(input())
                        if trans > int(fetch_bal(acc_num)):
                            print('Not enough money')
                        else:
                            cur.execute('UPDATE card SET balance = balance - ? WHERE number = ?', (trans, acc_num))
                            cur.execute('UPDATE card SET balance = balance + ? WHERE number = ?', (trans, trans_num))
                            conn.commit()
                            print('Success!')
                elif choice == 4:
                    cur.execute('DELETE FROM card WHERE number = ?', (acc_num,))
                    conn.commit()
                    print('The accounts has been closed!')
                elif choice == 5:
                    break
                elif choice == 0:
                    state = False
                    break
        else:
            print('wrong')


while state:
    print('1. Create an account \n2. Log into account \n0. Exit')
    choice = int(input())
    if choice == 0:
        print('Bye!')
        state = False
    if choice == 1:
        create_acc()
    if choice == 2:
        login()
    if choice == 0:
        state = False
